﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {
	public float startTime = 0.0f; 
	private Animator animator; 
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= startTime) {
			animator.SetTrigger ("Start"); 
		}
	}

	void Destroy(){
		Destroy (gameObject); 
	}

	void OnCollisionEnter(Collision collision){
		animator.SetTrigger ("Hit"); 
	}
}
